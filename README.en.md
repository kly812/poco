# poco

#### Description

#### Software Architecture
Software architecture description

POCO (Portable Components) C++ Libraries are:
 - A collection of C++ class libraries, conceptually similar to the Java Class Library or the .NET Framework.
- Focused on solutions to frequently-encountered practical problems.
- Focused on "internet-age" network-centric applications.
- Written in efficient, modern, 100% ANSI/ISO Standard C++.
- Based on and complementing the C++ Standard Library/STL.
- Highly portable and available on many different platforms, from embedded to server.
- Open Source, licensed under the Boost Software License.


#### Installation

1. Dowload RPM

wget http://121.36.3.168:82/home:/davidhan:/branches:/openEuler:/20.09:/Epol/standard_aarch64/aarch64/poco-1.10.1-1.oe1.aarch64.rpm

2. Install RPM

sudo rpm -ivh poco-1.10.1-1.oe1.aarch64.rpm

#### Instructions

Exit the console_bridge directory  under the /usr/local directory , Prove that the software installation is successful

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
