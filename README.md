# poco

#### 介绍
POCO C++是一个开源的C++类库的集合，它主要提供简单的、快速的网络和可移植应用程序的C++开发，这个类库和C++标准库可以很好的集成并填补C++标准库的功能空缺。POCO库的模块化、高效的设计及实现使得POCO特别适合嵌入式开发

#### 软件架构
软件架构说明

POCO（便携式组件）C ++库包括
- C ++类库的集合，概念上类似于Java类库或 .NET Framework。
- 专注于解决经常遇到的实际问题。
- 专注于以network-centric为中心的“internet-age”。
- 用高效，现代的100％ANSI / ISO标准C ++编写。
- 基于并补充了C ++标准库/ STL。
- 高度可移植，可在从嵌入式到服务器的许多不同平台上使用。
- 开源，根据Boost软件许可许可。

#### 安装教程

1. 下载rpm包

wget http://121.36.3.168:82/home:/davidhan:/branches:/openEuler:/20.09:/Epol/standard_aarch64/aarch64/poco-1.10.1-1.oe1.aarch64.rpm

2. 安装rpm包

sudo rpm -ivh poco-1.10.1-1.oe1.aarch64.rpm


#### 使用说明

安装完成以后，在/usr/local目录下有poco/文件夹证明安装成功

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
