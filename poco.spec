Name:		poco
Version:	1.10.1
Release:	4
Summary:	This is ROS melodic ros_comm's 3rdparty poco
License:	BSL-1.0
URL:		https://github.com/pocoproject/poco/releases/tag/poco-1.10.1-release
Source0:	https://github.com/pocoproject/poco/archive/poco-1.10.1-release.tar.gz
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	openssl-devel

%description
This is ROS melodic ros_comm's 3rdparty poco.

%prep
%setup

%install
mkdir cmake-build/
cd cmake-build/
cmake ..
make -j9 
make install
cd ..

#install
mkdir -p %{buildroot}/usr/local/
cp -r install/* %{buildroot}/usr/local/

find %{buildroot} -type f -name '*.so.*' -exec strip '{}' ';'

%files
%defattr(-,root,root)
/usr/local/*

%changelog
* Mon Aug 8 2022 Chenyx <chenyixiong3@huawei.com> - 1.10.1-4
- License compliance rectification

* Mon Dec 20 2021 sdlzx <hdu_sdlzx@163.com> - 1.10.1-3

* Sat Sep 4 2021 zhangtao <zhangtao221@huawei.com> - 1.10.1-2
- strip dynamic library
* Thu December 31 2020 openEuler Buildteam <hanhaomin008@126.com>
- Package init
